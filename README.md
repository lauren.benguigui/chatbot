Chatbot

Le programme Node.js permet l'execution de Javascript cotés serveur.
Installation de node:
Si vous souhiatez connaitre la version actuelle entrez dans le terminal : -node -v
--> Si un message d'erreur est renvoyé il vous faudra installer node. 
Entrez alors : npm install node
Pour cloner un repot :
Se rendre sur le lien : https://gitlab.com/lauren.benguigui/chatbot.git

--> Copier le lien https
Dans le terminal : --> Git clone 
Pour tester : dans le terminal faite tourner :  $ node server.js
Ouvrir page http://localhost:3000/
Installer les dépendances:
Pour les installer deux commandes sont nécéssaires :

1) npm install pour le node_modules
2) npm install express --save pour Express

Par la suite faite tourner le serveur.js : $ npm start server.js
Entrez dans votre navigateure votre localhost (localhost:3000/)
Si le Hello World s'affiche c'est que vous avez réussi

